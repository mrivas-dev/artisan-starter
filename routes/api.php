<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::view('/', 'welcome');

Route::group(['prefix' => 'auth'], function () {
	Route::post('login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout');
	Route::post('refresh', 'Auth\LoginController@refresh');
	Route::get('me', 'Auth\LoginController@me');
});

Route::middleware('auth:api')->group(function() {

});
