<?php

namespace App\Http\Controllers;

use App\Models\User;
use Hash;
use Illuminate\Http\Request;
use Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		if (empty($request->role)) {
			$users = User::query();
		} else {
			$users = User::role([$request->role]);
		}

		if (!empty($request->texto)) {
			$search = '%' . $request->texto . '%';
			if (is_numeric($request->texto)) {
				$users = $users->where('dni', 'LIKE', $search);
			} else {
				$users = $users->where('name', 'LIKE', $search)
					->orWhere('surname', 'LIKE', $search)
					->orWhere('email', 'LIKE', $search);
			}
		}
		
		$users = $users->paginate($request->cuantos, ['*'], 'pagina', $request->pag + 1);
		$users->getCollection()->transform(function ($user) {
			$roles = $user->getRoleNames();
			unset($user['roles']);
			$user['role'] = $roles[0];
			$user['display_role'] = ucwords(str_replace('_', ' ', $roles[0]));
			return $user;
		});
		return $users;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$request->dni = strval(intval($request->dni));

		$validator = Validator::make($request->all(), [
			'name' => 'required|string|min:1|max:50',
			'surname' => 'string|min:1|max:50',
			'email' => 'required|email|unique:users',
			'dni' => 'required|numeric|digits_between:7,8|unique:users',
			'role' => 'required|string|exists:roles,name'
		]);

		if ($validator->fails()) {
			return [
				'success' => false,
				'message' => $validator->errors()->first()
			];
		}

		$user = User::create([
			'name' => $request->name,
			'surname' => $request->surname,
			'email' => $request->email,
			'dni' => $request->dni
		]);
		$user->assignRole($request->role);
		
		return ['success' => true];
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Models\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function show(User $user)
	{
		$roles = $user->getRoleNames();
		unset($user['roles']);
		$user['role'] = $roles[0];
		$user['display_role'] = ucwords(str_replace('_', ' ', $roles[0]));
		return $user;
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, User $user)
	{
		if (!auth()->user()->can('gestionar usuarios')) {
			return response()->json(['success' => false, 'message' => 'No tenés permiso para gestionar usuarios'], 403);
		}
		
		if (auth()->user()->role === 'gerencia' && $user->role === 'admin') {
			return response()->json(['success' => false, 'message' => 'No podés editar a un administrador'], 403);
		}

		$request->dni = strval(intval($request->dni));

		$validator = Validator::make($request->all(), [
			'name' => 'required|string|min:1|max:50',
			'surname' => 'string|min:1|max:50',
			'email' => 'required|email|unique:users',
			'role' => 'required|string|exists:roles,name'
		]);

		if ($validator->fails()) {
			return ['success' => false, 'message' => $validator->errors()->first()];
		}

		if ($request->password) {
			$user->password = Hash::make($request->password);
		}

		if ($request->role !== $user->role) {
			$user->removeRole($user->role);
			$user->assignRole($request->role);
		}

		$user->name = $request->name;
		$user->surname = $request->surname;
		$user->email = $request->email;
		$user->save();
		return ['success' => true];
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\User  $user
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(User $user)
	{
		//
	}

	public function getRoles()
	{
		$roles = Role::orderBy('name')->get();
		return $roles->map(function ($role) {
			$role['display'] = ucwords(str_replace('_', ' ', $role['name']));
			return $role->toArray();
		});
	}
}
